<?php

return [
    'model_defaults' => [
	    'namespace'       => 'App\Http\Models',
        'base_class_name' => \Illuminate\Database\Eloquent\Model::class,
    ],

    'db_types' => [
        'enum' => 'string',
    ],
];