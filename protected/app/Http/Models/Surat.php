<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_surat
 * @property string $kode
 * @property string $perihal
 * @property string $sinopsis
 * @property string $scan
 * @property string $tgl_surat
 * @property string $created_at
 * @property string $updated_at
 * @property Koneksi[] $koneksis
 */
class Surat extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'surat';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_surat';

    /**
     * @var array
     */
    protected $fillable = ['kode', 'perihal', 'sinopsis', 'scan', 'tgl_surat', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function koneksis()
    {
        return $this->hasMany('App\Http\Models\Koneksi', 'id_surat', 'id_surat');
    }
}
