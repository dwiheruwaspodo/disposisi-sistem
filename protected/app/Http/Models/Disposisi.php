<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_disposisi
 * @property int $created_by
 * @property int $id_koneksi
 * @property string $disposisi
 * @property string $created_at
 * @property string $updated_at
 * @property Koneksi $koneksi
 * @property User $user
 * @property Feedback[] $feedback
 */
class Disposisi extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'disposisi';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_disposisi';

    /**
     * @var array
     */
    protected $fillable = ['created_by', 'id_surat', 'disposisi', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function koneksi()
    {
        return $this->belongsTo('App\Http\Models\Koneksi', 'id_koneksi', 'id_koneksi');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Http\Models\User', 'created_by', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedback()
    {
        return $this->hasMany('App\Http\Models\Feedback', 'id_disposisi', 'id_disposisi');
    }
}
