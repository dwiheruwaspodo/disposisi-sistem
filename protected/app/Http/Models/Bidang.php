<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_bidang
 * @property string $bidang
 * @property string $created_at
 * @property string $updated_at
 * @property Feedback[] $feedback
 * @property Koneksi[] $koneksis
 * @property User[] $users
 */
class Bidang extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'bidang';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_bidang';

    /**
     * @var array
     */
    protected $fillable = ['bidang', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedback()
    {
        return $this->hasMany('App\Http\Models\Feedback', 'id_bidang', 'id_bidang');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function koneksis()
    {
        return $this->hasMany('App\Http\Models\Koneksi', 'id_bidang', 'id_bidang');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\Http\Models\User', 'id_bidang', 'id_bidang');
    }
}
