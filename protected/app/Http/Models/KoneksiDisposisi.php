<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $id_disposisi
 * @property int $id_koneksi
 * @property string $created_at
 * @property string $updated_at
 * @property Koneksi $koneksi
 * @property Disposisi $disposisi
 */
class KoneksiDisposisi extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'koneksi_disposisi';

    /**
     * @var array
     */
    protected $fillable = ['id_disposisi', 'id_koneksi', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function koneksi()
    {
        return $this->belongsTo('App\Http\Models\Koneksi', 'id_koneksi', 'id_koneksi');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function disposisi()
    {
        return $this->belongsTo('App\Http\Models\Disposisi', 'id_disposisi', 'id_disposisi');
    }
}
