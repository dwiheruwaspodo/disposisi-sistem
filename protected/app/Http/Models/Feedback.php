<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_feedback
 * @property int $id_disposisi
 * @property int $id_bidang
 * @property string $feedback
 * @property string $created_at
 * @property string $updated_at
 * @property Bidang $bidang
 * @property Disposisi $disposisi
 */
class Feedback extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_feedback';

    /**
     * @var array
     */
    protected $fillable = ['id_disposisi', 'id_bidang', 'feedback', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bidang()
    {
        return $this->belongsTo('App\Http\Models\Bidang', 'id_bidang', 'id_bidang');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function disposisi()
    {
        return $this->belongsTo('App\Http\Models\Disposisi', 'id_disposisi', 'id_disposisi');
    }
}
