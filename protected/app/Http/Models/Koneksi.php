<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_koneksi
 * @property int $id_surat
 * @property int $id_bidang
 * @property string $created_at
 * @property string $updated_at
 * @property Bidang $bidang
 * @property Surat $surat
 * @property Disposisi[] $disposisis
 */
class Koneksi extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'koneksi';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_koneksi';

    /**
     * @var array
     */
    protected $fillable = ['id_surat', 'id_bidang', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bidang()
    {
        return $this->belongsTo('App\Http\Models\Bidang', 'id_bidang', 'id_bidang');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function surat()
    {
        return $this->belongsTo('App\Http\Models\Surat', 'id_surat', 'id_surat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function disposisis()
    {
        return $this->hasMany('App\Http\Models\Disposisi', 'id_koneksi', 'id_koneksi');
    }
}
