<?php
 
use App\Http\Models\Notif;
use App\Http\Models\User;

function notification() {
	$notif = queryNotification();

	if (!empty($notif)) {

		foreach ($notif as $key => $value) {
			echo '<li>';
				if ($value['status'] == '0') {
	              	echo '<a href="'.url('disposisi/read', $value['id_notif']).'">';
				}
				else {
	              	echo '<a href="'.url('disposisi').'">';
				}

				if ($value['status'] == '0') {
	              	echo '<i class="fa fa-circle text-aqua"></i> '.$value['notif'];
				}
				else {
	              	echo $value['notif'];
				}
              echo '</a>';
            echo '</li>';
		}
	}

}

function queryNotification() {
	$job = Notif::where('id_bidang', Auth::user()->id_bidang)->get()->toArray();

	return $job;
}

function newqueryNotification() {
	$job = Notif::where('id_bidang', Auth::user()->id_bidang)->where('status', '=', '0')->get()->toArray();

	return $job;
}
 
?>