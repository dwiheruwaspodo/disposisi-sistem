<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers;

use App\Http\Models\User;
use App\Http\Models\Bidang;
use App\Http\Models\Jabatan;

use Hash;

class UserController extends Controller
{
    /**
     * tambah user
     */
    function create(Request $request) {
    	$post = $request->except('_token');

    	if (empty($post)) {
    		$data = [
				'title'    => 'Tambah User',
				'menu'     => 'user',
				'sub_menu' => 'user tambah'
    		];

    		// ambil bidang
			$bidang         = Bidang::get()->toArray();

			if (empty($bidang)) {
				return redirect('bidang')->withErrors(['Mohon melengkapi data bidang.']);
			}
			else {
				$data['bidang'] = $bidang;
			}

    		// ambil jabatan
    		$jabatan = Jabatan::get()->toArray();

    		if (empty($jabatan)) {
    			return redirect('jabatan')->withErrors(['Mohon melengkapi data jabatan.']);
    		}
    		else {
    			$data['jabatan'] = $jabatan;
    		}

    		return view('content.user.create', $data);
    	}
    	else {
    		
    		$post = array_filter($post);

    		$post['password'] = Hash::make($post['npk']);
    		$save = User::create($post);

    		return parent::redirect($save, 'Data user berhasil ditambahkan.');
    	}
    }

    /**
     * list
     */
    function index(Request $request) {
		$data = [
			'title'    => 'List User & Admin',
			'menu'     => 'user',
			'sub_menu' => 'user list'
		];

		$data['user'] = User::with(['jabatan','bidang'])->get()->toArray();

		// print_r($data); exit();
		return view('content.user.list', $data);
    }

    /**
     * delete
     */
    function delete(Request $request) {
		$post   = $request->except('_token');
		
		$delete = User::where('id_bidang', $post['id_bidang'])->delete();

    	return parent::redirect($delete, 'Data user berhasil dinon-aktifkan.');
    }

    /**
     * update
     */
    function update(Request $request, $id) {
    	$post = $request->except('_token');

    	if (empty($post)) {
    		$data = [
    			'title'    => 'List User & Admin',
    			'menu'     => 'user',
    			'sub_menu' => 'user list'
    		];

    		// ambil bidang
			$bidang         = Bidang::get()->toArray();

			if (empty($bidang)) {
				return redirect('bidang')->withErrors(['Mohon melengkapi data bidang.']);
			}
			else {
				$data['bidang'] = $bidang;
			}

    		// ambil jabatan
    		$jabatan = Jabatan::get()->toArray();

    		if (empty($jabatan)) {
    			return redirect('jabatan')->withErrors(['Mohon melengkapi data jabatan.']);
    		}
    		else {
    			$data['jabatan'] = $jabatan;
    		}

    		// ambil user
    		$user = User::with(['jabatan','bidang'])->where('npk', $id)->get()->toArray();

    		if (empty($user)) {
    			return back()->withErrors(['Data user tidak ditemukan.']);
    		}
    		else {
    			// ambil array ke 0
    			$data['user'] = $user;

    			// print_r($data); exit();
    			return view('content.user.update', $data);
    		}
    	}
    	else {

    		if (!isset($post['is_admin'])) {
    			$post['is_admin'] = '0';
    		}

            if (isset($post['deleted_at']) && $post['deleted_at'] == "no") {
                $post['deleted_at'] = null;
            }

    		$update = User::where('id_user', $post['id_user'])->update($post);

    		return parent::redirect($update, 'Data user berhasil diperbarui.', 'user');
    	}
    }
}
