<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;


use App\Http\Models\Notif;
use App\Http\Models\User;
use App\Http\Models\Koneksi;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function redirect($save, $messagesSuccess, $next=null) {
        if ($save) {
            if (is_null($next)) {
                return back()->with('success', [$messagesSuccess]);
            }
            else {
                return redirect($next)->with('success', [$messagesSuccess]);
            }
        }
        else {
            $e = ['e' => 'Terjadi kesalahan. Silakan coba lagi.'];
            return back()->witherrors($e)->withInput();
        }
    }

    function saveNotif($id_surat, $post) {

        $koneksi = Koneksi::where('id_surat', $id_surat)->get()->toArray();
        
        if (!empty($koneksi)) {
            $notif = [];

            foreach ($koneksi as $key => $value) {
                $data = [
                    'notif'      => $post['notif'],
                    'type'       => $post['type'],
                    'status'     => '0',
                    'id_bidang'  => $value['id_bidang'],
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];        

                array_push($notif, $data);
            }

            $save = Notif::insert($notif);
            
            return $save;            
        }

        return true;
    }

    function updateStatusRead($id_notif, $status) {
        $data = [
            'status' => $status
        ];

        $update = Notif::where('id_notif', $id_notif)->update($data);

        return $update;
    }
}
