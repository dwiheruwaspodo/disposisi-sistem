@extends('template.body')

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>Bidang</li>
	    	<li class="active">Tambah</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form class="form-horizontal" action="{{ url()->current() }}" method="POST">
	      	<div class="box-body">
		        <div class="form-group">
		          <label for="inputEmail3" class="col-md-2 control-label">Nama Bidang</label>

		          <div class="col-md-10">
		            <input type="text" class="form-control" name="bidang" placeholder="Nama Bidang" required maxlength="100" value="{{ $bidang['bidang'] }}">
		          </div>
		        </div>
	      	</div>
	      <!-- /.box-body -->
	    <div class="box-footer">
			<div class="col-md-2">		
			</div>
			<div class="col-md-10">
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-info">Submit</button>
				{{ csrf_field() }}
				<input type="hidden" name="id_bidang" value="{{ $bidang['id_bidang'] }}">	
			</div>
	    </div>
	      <!-- /.box-footer -->
	    </form>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')

@endsection 