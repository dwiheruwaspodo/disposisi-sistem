@extends('template.body')

@section('style')
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>Disposisi</li>
	    	<li class="active">Detail</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form class="form-horizontal" action="{{ url()->current() }}" method="POST">
	    @foreach ($disposisi as $su)
	      	<div class="box-body">
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Kode Surat</label>

		          	<div class="col-md-10">
		            	<input type="text" class="form-control" value="{{ $su['kode'] }}" readonly>
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Tanggal Surat</label>

		          	<div class="col-md-10">
		            	<input type="text" class="form-control" value="{{ date('d F Y', strtotime($su['tgl_surat'])) }}" readonly>
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Perihal</label>

		          	<div class="col-md-10">
		            	<input type="text" class="form-control" value="{{ $su['perihal'] }}" readonly>
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Sinopsis</label>

		          	<div class="col-md-10">
			          	<textarea class="form-control" readonly> {{ $su['sinopsis'] }} </textarea>
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Disposisi</label>

		          	<div class="col-md-10">
			          	<textarea class="form-control" readonly> {{ $su['disposisi'] }} </textarea>
		          	</div>
		        </div>
                <div class="form-group">
                  	<label class="col-md-2 control-label">Dibuat oleh</label>

                  	<div class="col-md-10">
        	          	<input class="form-control" value="{{ $su['name'] }}" readonly>
                  	</div>
                </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Bidang</label>
		          	<div class="col-md-10">
		          	@if (empty($su['bidang']))
						-
		          	@else
			          	@foreach ($su['bidang'] as $yuw)
				          	<li>{{ $yuw['bidang']['bidang'] }}</li>
				        @endforeach
		          	@endif
		          </div>
		        </div>
		       
	      	</div>
	    @endforeach
	      <!-- /.box-body -->
	    <div class="box-footer">
			<div class="col-md-2">		
			</div>
			<div class="col-md-10">
				<a href="{{ url($su['scan']) }}" class="btn btn-info">Download Surat</a>
			</div>
	    </div>
	      <!-- /.box-footer -->
	    </form>
	</div>

</section>
<!-- /.content -->


@endsection

