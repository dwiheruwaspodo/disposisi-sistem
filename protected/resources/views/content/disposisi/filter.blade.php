@extends('template.body')

@section('style')
	
	<link rel="stylesheet" href="{{ url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css">') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>Disposisi</li>
	    	<li class="active">List</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	

	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form class="form-horizontal" action="{{ url()->current() }}" method="POST" enctype="multipart/form-data">
	      	<div class="box-body">
		        <div class="form-group">
		          <label for="inputEmail3" class="col-md-2 control-label">Kode Surat </label>

		          <div class="col-md-10">
		            <input type="text" class="form-control" name="kode" placeholder="Kode Surat" required maxlength="100" value="{{ old('kode') }}">
		          </div>
		        </div>

	      	</div>
	      <!-- /.box-body -->
	    <div class="box-footer">
			<div class="col-md-2">		
			</div>
			<div class="col-md-10">
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-info">Cari</button>
				{{ csrf_field() }}	
			</div>
	    </div>
	      <!-- /.box-footer -->
	    </form>
	</div>

</section>
<!-- /.content -->
@endsection