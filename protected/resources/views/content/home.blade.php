@extends('template.body')

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home active"></i> Home</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
  	<!-- Default box -->
	<div class="box">
	  	<div class="box-header with-border">
		  <h3 class="box-title">Welcome {{ Auth::user()->name }}</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		  <table class="table table-bordered">
		    
			@php
				$show = ['name', 'email', 'phone', 'npk', 'jabatan', 'bidang', 'is_admin'];	
			@endphp

		    @foreach ($info[0] as $key=>$val)
		    <tr>

		    	@if (in_array($key, $show))
			    	<td><strong> {{ ucwords($key) }} </strong></td>
					<!-- jika jabatan nggak kosong -->
		    		@if ($key == 'jabatan')
						@if (!empty($val))
							<td> {{ $val['jabatan'] }}</td>
						@endif
		    						
					<!-- jika bidang nggak kosong -->
		    		@elseif ($key == 'bidang')
						@if (!empty($val))
							<td> {{ $val['bidang'] }}</td>	
						@endif

					<!-- jika admin -->
					@elseif ($key == 'is_admin')
						@if ($val == 1)
							<td> Yes </td>
						@else
							<td> No </td>
						@endif
					@else
						<!-- selainnya -->
						<td> {{ $val }}</td>
					@endif
		    	@endif
		    </tr>
		    @endforeach

		  </table>
		</div>
	    
	    <!-- /.box-body -->
	    <!-- <div class="box-footer">
	      Footer
	    </div> -->
	    <!-- /.box-footer-->
	</div>
  	<!-- /.box -->
</section>
<!-- /.content -->


@endsection

@section('script')

@endsection 