@extends('template.body')

@section('style')
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>User</li>
	    	<li class="active">Tambah</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <!-- /.box-header -->
	    <!-- form start -->
	    <form class="form-horizontal" action="{{ url()->current() }}" method="POST">
	    @foreach ($user as $su)
	      	<div class="box-body">
		        <div class="form-group">
		          	<label class="col-md-2 control-label">NPK</label>

		          	<div class="col-md-10">
		            	<input type="text" class="form-control" name="npk" placeholder="NPK" required maxlength="100" value="{{ $su['npk'] }}">
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Nama</label>

		          	<div class="col-md-10">
		            	<input type="text" class="form-control" name="name" placeholder="Nama" required maxlength="100" value="{{ $su['name'] }}">
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Email</label>

		          	<div class="col-md-10">
		            	<input type="email" class="form-control" name="email" placeholder="Email" maxlength="100" value="{{ $su['email'] }}">
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Phone</label>

		          	<div class="col-md-10">
		            <input type="text" class="form-control" name="phone" placeholder="phone" required maxlength="100" value="{{ $su['phone'] }}">
		          	</div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Bidang</label>
		          	<div class="col-md-10">
			          	<select class="form-control select2" data-placeholder="Pilih Bidang" name="id_bidang">
							<option value=""> </option>
		          		@foreach($bidang as $val)
							<option value="{{ $val['id_bidang'] }}" @if ($su['id_bidang'] == $val['id_bidang']) selected @endif> {{ $val['bidang'] }}</option>
						@endforeach
						</select>
		          </div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Jabatan</label>
		          	<div class="col-md-10">
			          	<select class="form-control select2" data-placeholder="Pilih Jabatan" name="id_jabatan">
							<option value=""> </option>
		          		@foreach($jabatan as $val)
							<option value="{{ $val['id_jabatan'] }}" @if ($su['id_jabatan'] == $val['id_jabatan']) selected @endif> {{ $val['jabatan'] }}</option>
						@endforeach
						</select>
		          </div>
		        </div>
		        <div class="form-group">
		          	<label class="col-md-2 control-label">Admin</label>
					<div class="col-md-10">
			          	<div class="checkbox">
	                      <label>
	                        <input type="checkbox" name="is_admin" @if($su['is_admin'] == 1) checked @endif value="1">
	                        Ya
	                      </label>
	                    </div>
                    </div>
		        </div>
	      	</div>
		<input type="hidden" name="id_user" value="{{ $su['id_user'] }}">
	    @endforeach
	      <!-- /.box-body -->
	    <div class="box-footer">
			<div class="col-md-2">		
			</div>
			<div class="col-md-10">
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-info">Submit</button>
				{{ csrf_field() }}	
			</div>
	    </div>
	      <!-- /.box-footer -->
	    </form>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
<script src="{{ url('bower_components/select2/dist/js/select2.full.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.select2').select2();
	});
</script>
@endsection 