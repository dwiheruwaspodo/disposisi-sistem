@extends('template.body')

@section('style')
	
	<link rel="stylesheet" href="{{ url('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ url('bower_components/select2/dist/css/select2.min.css">') }}">
@endsection

@section('content')		 
<!-- Content Header (Page header) -->
<section class="content-header">
  	<div>
	    <ol class="breadcrumb">
	    	<li><i class="fa fa-home"></i> Home</li>
	    	<li>User</li>
	    	<li class="active">List</li>
	  	</ol>
	</div>
</section>

<!-- Main content -->
<section class="content">
	@include('template.alert')
	
	<div class="box">
	    <div class="box-header with-border">
	      <h3 class="box-title">{{ $title }}</h3>
	    </div>
	    <div class="box-body">
	      <table id="table01" class="table table-bordered table-striped">
	        <thead>
	        <tr>
	          	<th>Nama</th>
	          	<th>NPK</th>
	          	<th>Email</th>
	          	<th>Phone</th>
	          	<th>Status</th>
	          	<th>Bidang</th>
	          	<th>Jabatan</th>
	          	<th>Action</th>
	        </tr>
	        </thead>
	        <tbody>
	        @if (!empty($user))
		        @foreach ($user as $key=>$val)
		        	<tr>
			          	<td>{{ $val['name'] }}</td>
			          	<td>{{ $val['npk'] }}</td>
			          	<td>{{ $val['email'] }}</td>
			          	<td>{{ $val['phone'] }}</td>
			          	<td> @if ($val['is_admin'] == 1) Admin @else Non-admin @endif </td>
			          	<td> @if (!empty($val['bidang'])) {{$val['bidang']['bidang']}} @else - @endif </td>
			          	<td> @if (!empty($val['jabatan'])) {{$val['jabatan']['jabatan']}} @else - @endif </td>
			          	<td>
			          		<a href="{{ url('user/update', $val['npk']) }}" class="btn btn-success"><i class="fa fa-edit"></i> Update </a>
			          		<!-- make admin -->
			          		@if ($val['is_admin'] == 1) 
			          			<button class="btn btn-warning admin" data-toggle="confirmation-popout" data-popout="true" data-id="{{ $val['id_user'] }}"> <i class="fa fa-user"></i> </button>

			          			<form id="admin-{{ $val['id_user'] }}" action="{{ url('user/update', $val['npk']) }}" method="POST" style="display: none;">
			          			    {{ csrf_field() }}
			          			    <input type="hidden" name="id_user" value="{{ $val['id_user'] }}">
			          			    <input type="hidden" name="is_admin" value="0">
			          			</form>
			          		@else
			          			<button class="btn btn-warning admin" data-toggle="confirmation-popout" data-popout="true" data-id="{{ $val['id_user'] }}"> <i class="fa fa-user-plus"></i> </button>

			          			<form id="admin-{{ $val['id_user'] }}" action="{{ url('user/update', $val['npk']) }}" method="POST" style="display: none;">
			          			    {{ csrf_field() }}
			          			    <input type="hidden" name="is_admin" value="1">
			          			    <input type="hidden" name="id_user" value="{{ $val['id_user'] }}">
			          			</form>
			          		@endif

			          		<!-- banned -->
			          		@if (empty($val['deleted_at'])) 
			          			<button class="btn btn-danger delete" data-toggle="confirmation-popout" data-popout="true" data-id="{{ $val['id_user'] }}"> <i class="fa fa-ban"></i> </button>

			          			<form id="delete-{{ $val['id_user'] }}" action="{{ url('user/update', $val['npk']) }}" method="POST" style="display: none;">
			          			    {{ csrf_field() }}
			          			    <input type="hidden" name="deleted_at" value="{{ date('Y-m-d H:i:s') }}">
			          			    <input type="hidden" name="id_user" value="{{ $val['id_user'] }}">
			          			</form>
			          		@else
			          			<button class="btn btn-info delete" data-toggle="confirmation-popout" data-popout="true" data-id="{{ $val['id_user'] }}"> <i class="fa fa-check"></i> </button>

			          			<form id="delete-{{ $val['id_user'] }}" action="{{ url('user/update', $val['npk']) }}" method="POST" style="display: none;">
			          			    {{ csrf_field() }}
			          			    <input type="hidden" name="deleted_at" value="no">
			          			    <input type="hidden" name="id_user" value="{{ $val['id_user'] }}">
			          			</form>
			          		@endif

			          		
			          	</td>
			        </tr>
			    @endforeach
		    @endif
	        </tbody>
	      </table>
	    </div>
	</div>

</section>
<!-- /.content -->


@endsection

@section('script')
<script src="{{ url('bower_components/bootstrap-confirmation/bootstrap-confirmation.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script src="{{ url('js/bootstrap-confirmation.js') }}"></script>
<script>
  $(function () {
    $('#table01').DataTable();
    
  });
</script>
<script type="text/javascript">
	$('[data-toggle=confirmation-popout]').confirmation({
	    rootSelector: '[data-toggle=confirmation-popout]',
	    container: 'body'
	});
</script>
<script type="text/javascript">
	$('#table01').on('click', '.delete', function() {
		$('#delete-'+$(this).data('id')).submit();
	});

	$('#table01').on('click', '.admin', function() {
		$('#admin-'+$(this).data('id')).submit();
	});
</script>

@endsection 