@if($errors->any())
  @if($errors->all()[0] != 'empty')
  	<div class="alert alert-warning alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h4><i class="icon fa fa-warning"></i> Warning!</h4>
      @foreach($errors->all() as $e)
        - <?php echo $e ?> <br/>
      @endforeach
    </div>
 @endif
@endif

@if(session()->exists('success'))
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<h4><i class="icon fa fa-check"></i> Success!</h4>
		@foreach(session('success') as $s)
		  - {!! $s !!} <br/>
		@endforeach
	</div>
 <?php Session::forget('success'); ?>
@endif
