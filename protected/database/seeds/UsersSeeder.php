<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        
        \DB::table('users')->insert([
    		[
    			'id_user'    => 1,
    			'name'       => 'Admin',
    			'email'      => 'admin@sistem.com',
    			'phone'      => '0811223344',
    			'npk'        => '1234567890',
    			'password'   => Hash::make('123123'),
    			'is_admin'   => 1,      
    			'created_at' => date('Y-m-d H:i:s'),
    			'updated_at' => date('Y-m-d H:i:s'),
    		]
        ]);
    }
}
