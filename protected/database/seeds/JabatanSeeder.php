<?php

use Illuminate\Database\Seeder;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('jabatan')->delete();
        
        \DB::table('jabatan')->insert([
    		[
				'id_jabatan' => 1,
                'jabatan'    => 'Kepala Kantor Cabang', 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_at' => date('Y-m-d H:i:s')],
    		[
				'id_jabatan' => 2,
                'jabatan'    => 'Kepala Bidang', 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_at' => date('Y-m-d H:i:s')],
    		[
				'id_jabatan' => 3,
                'jabatan'    => 'Sekretaris', 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_at' => date('Y-m-d H:i:s')],
        ]);
    }
}
