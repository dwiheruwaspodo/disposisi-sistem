<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id_feedback');
            $table->text('feedback');
            // disposisi
            $table->unsignedInteger('id_disposisi');
            $table->foreign('id_disposisi')->references('id_disposisi')->on('disposisi')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            // relasi bidang
            $table->unsignedInteger('id_bidang');
            $table->foreign('id_bidang')->references('id_bidang')->on('bidang')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
