<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKoneksiDisposisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koneksi_disposisi', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_disposisi');
            $table->foreign('id_disposisi')->references('id_disposisi')->on('disposisi')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->unsignedInteger('id_koneksi');
            $table->foreign('id_koneksi')->references('id_koneksi')->on('koneksi')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->timestamps();
        });

        // delete id koneksi
        Schema::table('disposisi', function(Blueprint $table) {
            $table->dropForeign('disposisi_id_koneksi_foreign');
            $table->dropIndex('disposisi_id_koneksi_foreign');
            $table->dropColumn('id_koneksi');

            // create relasi surat
            // relasi surat untuk mempermudah
            $table->unsignedInteger('id_surat');
            $table->foreign('id_surat')->references('id_surat')->on('surat')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koneksi_disposisi');

        Schema::table('disposisi', function(Blueprint $table) {
            $table->unsignedInteger('id_koneksi');
            $table->foreign('id_koneksi')->references('id_koneksi')->on('koneksi')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');  

            $table->dropForeign('disposisi_id_surat_foreign');
            $table->dropIndex('disposisi_id_surat_foreign');
            $table->dropColumn('id_surat');
        });
    }
}
