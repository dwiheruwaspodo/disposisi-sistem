<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id_user');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('npk', 50)->unique();
            $table->string('password');
            $table->enum('is_admin', [1, 0])->default(0);
            // relasi bidang
            $table->unsignedInteger('id_bidang')->nullable();
            $table->foreign('id_bidang')->references('id_bidang')->on('bidang')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            // relasi jabatan
            $table->unsignedInteger('id_jabatan')->nullable();
            $table->foreign('id_jabatan')->references('id_jabatan')->on('jabatan')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
