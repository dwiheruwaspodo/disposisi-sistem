<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusReadIdReferences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notif', function(Blueprint $table) {
            $table->enum('status', [1, 0])->nullable()->after('type');
            $table->string('id_references', 100)->nullable();

            $table->dropForeign('notif_id_user_foreign');
            $table->dropIndex('notif_id_user_foreign');
            $table->dropColumn('id_user');

            $table->unsignedInteger('id_bidang');
            $table->foreign('id_bidang')->references('id_bidang')->on('bidang')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notif', function(Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('id_references');

            $table->unsignedInteger('id_user');
            $table->foreign('id_user')->references('id_user')->on('users')
                            ->onDelete('cascade')
                            ->onUpdate('cascade'); 

            $table->dropColumn('id_bidang');
             
        });
    }
}
